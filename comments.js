jQuery(function($) {


  /**
   * I have wrapped a set of common methods that access the API in an object called comments, the comments object is attached to the window object so that I can easily access them regardless of scope.
   * @type {{endpoint: string, show_comments: Function, add_delete_events: Function, add_comment: Function}}
   */
  window.comments = {
    /** the URL contacted everytime we wish to access the API. */
    endpoint: 'https://teaching.computing.edgehill.ac.uk/cis1007/add-my-comment/23292491',


    
    /** this method gets the comments using a HTTP GET request, and adds the comments in HTML to the comments list */
    show_comments: function() {
      $.get(this.endpoint, function(response) {
        var commentsList = $('#comments-list');

        /* remove old comments as we are retrieving a full list */
        commentsList.find('li').remove();

        /* add each comment in HTML to the comments list */
        for (var i=0; i<response.length; i++) {
          var item = response[i];
          commentsList.append('<li>' +
                              '  <h4>' + item.name + ' (' + item.email + ') [<a class="delete-comment-link" data-id="' + item.id + '">delete</a>]</h4>' +
                              '  <p>' + item.comment + '</p>' +
                              '</li>');
        }

        /* attach click events to the newly added comments so they can be deleted individually. */
        comments.add_delete_events();
      });
    },


    
    /** this method adds a click event onto each delete link, used every time we call show_comments etc... */
    add_delete_events: function() {
      /* I use the off() method first just in-case the selected elements already have a click handler */
      $('.delete-comment-link').off('click').click(function() {
        var deleteLink = $(this);
        var comment_id = deleteLink.data('id');

        /* confirm the action just in-case the user clicked this by mistake */
        if (confirm('Are you sure?')) {
          /* make the ajax HTTP DELETE call to remove the comment. */
          $.ajax({
              url: comments.endpoint + "/" + comment_id,
              type: 'DELETE',
              success: function() {
                console.log('Deleted #' + comment_id);
                comments.show_comments();
              }
          });    
        }
      });
    },



    /**
     * Add a comment
     * @param name The name of the commenter.
     * @param email The email address.
     * @param comment The comment body.
     * @param callback A function to call if adding the comment was successful.
     */
    
    add_comment: function(name, email, comment, callback) {
      var args = {
        'name': name,
        'email': email,
        'comment': comment
      };
      $.post(this.endpoint, args, callback);
    }
  };


  
  
    
  /* Handle what happens when the form is submitted, and send the request to add a comment using ajax instead */
  $('#comments-form').on('submit', function(e) {
    
    /* stop the form from navigating away */
    e.preventDefault();
    
    /* collect the information */
    var form = $(this);
    var _name = form.find('input[name="name"]').val();
    var _email = form.find('input[name="email"]').val();
    var _comment = form.find('textarea[name="comment"]').val();
    
    /* send it to the add_comment method */
    comments.add_comment(_name, _email, _comment, function(response) {
      if (response.error) {
        alert(response.error);
      }
      else {
        comments.show_comments();
        console.log('Added Comment! #' + response.id);
        form[0].reset();
      }
    });
  });




  /* When the user clicks on an element with the class module-title, I read the data-target attribute (which contains
   * a selector for the associated element) and call slideToggle() on that associated element to show or hide it. */
  $('.module-title').click(function() {
    var target = $(this).data('target');
    $(target).slideToggle();
  });



  /* This click event collapses all modules that are open using the hide() method. */
  jQuery("#collapse-modules").click(function(){
    $('.m1c').hide();
  });
    
  
  /* When the page loads, immediately load in the comments */
  comments.show_comments();
});


