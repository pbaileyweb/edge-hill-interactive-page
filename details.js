
/* Old JS used as an onclick function in HTML, replaced with a toggle function for improved functionality
*
jQuery(function () {

	window.toggleInfo = function(selector) {
    jQuery(selector).toggleClass('active');
  };
});
*/


$(document).ready(function(){
    $("#btn1").click(function(){//when id #btn1 is clicked perform a function
        $("#moreinfo1").toggle(200); //function = #moreinfo1 is toggled either show/hide, at 200 speed
    });
    $("#btn2").click(function(){
        $("#moreinfo2").toggle(200);
    });
    $("#btn3").click(function(){
        $("#moreinfo3").toggle(200);
    });
    $("#btn4").click(function(){
        $("#moreinfo4").toggle(200);
    });
    $("#btn5").click(function(){
        $("#moreinfo5").toggle(200);
    });
});
